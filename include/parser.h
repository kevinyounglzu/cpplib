#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>
#include <vector>
#include <map>

namespace mylib
{
    // configuration parser
    class Parser
    {
        private:
            std::string _file_name;
            std::ifstream the_file;
            std::vector<std::string> vec_of_strings;
            std::map<std::string, int> map_int;
            std::map<std::string, double> map_double;
        public:
            Parser(std::string file_name);
            ~Parser() { the_file.close(); }

            std::string getFileName() { return _file_name; }

            Parser & parseTheFile();
            Parser & readFile(); // Read the file to a vector of strings
            const std::vector<std::string> & getVecOfStrings() const { return vec_of_strings; }
            void parseLine(std::string str); // split the string with ' ' and '='
            int getInt(std::string str) { return map_int[str]; }
            double getDouble(std::string str) { return map_double[str]; }
    };

    // file name tape
    template<typename T>
    std::string tapeFileName(std::string file_name_base, std::string file_name_suffix, T to_tape)
    {
        std::ostringstream str;
        str << file_name_base << to_tape << file_name_suffix;
        return str.str();
    }
}

#endif
