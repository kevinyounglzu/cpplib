#ifndef RAND_H
#define RAND_H

#include <random>

namespace mylib
{

    /* generate random int in [lower_bound, upper_bound]
     * Usage:
     *       RandInt(n)     =>   [0, n]
     *       RandInt(n, m)  =>   [n, m]
     *
     *       RandInt ri(..)
     *       ri.gen()
     *       */
    class RandInt
    {
        public:
            typedef std::uniform_int_distribution<std::mt19937::result_type> DistType;
        private:
            /* Use mt19937 as the default random engine. */
            std::mt19937 random_engine;
            DistType dist;

        public:
            /* Constructors */
            RandInt(int upper_bound): random_engine(), dist(0, upper_bound)
            {
                /* seed the random engine */
                random_engine.seed(std::random_device()());
            }

            RandInt(int lower_bound, int upper_bound): random_engine(), dist(lower_bound, upper_bound)
            {
                /* seed the random engine */
                random_engine.seed(std::random_device()());
            }

            /* Generate random numbers */
//            int gen() { return dist(random_engine); }
            int gen()
            {
                return dist(random_engine);
            }

            /* Generate random number on the fly with given bound */
            int genOTF(int lower_bound, int upper_bound)
            {
                return DistType(lower_bound, upper_bound)(random_engine);
            }
    };


    /* generate random double in [lower_bound, upper_bound) 
     * Usage:
     *       RandDouble(m)      => [0.0, m)
     *       RandDouble(n, m)   => [n, m)
     *       RandDouble()       => [0.0, 1)*/
    class RandDouble
    {
        public:
            typedef std::uniform_real_distribution<double> DistType;
        private:
            /* Use mt19937 as the default random engine. */
            std::mt19937 random_engine;
            DistType dist;
        public:
            /* Constructors */
            RandDouble(): random_engine(), dist(0.0, 1.0)
            {
                random_engine.seed(std::random_device()());
            }

            RandDouble(double upper_bound): random_engine(), dist(0.0, upper_bound)
            {
                random_engine.seed(std::random_device()());
            }

            RandDouble(double lower_bound, double upper_bound):random_engine(), dist(lower_bound, upper_bound)
            {
                random_engine.seed(std::random_device()());
            }

            double gen() { return dist(random_engine); }

            double genOTF(double lower_bound, double upper_bound)
            {
                return DistType(lower_bound, upper_bound)(random_engine);
            }
    };

    /* generate random number from normal distributions with mean mu
     * and sd sigma
     * Usage:
     *          RandNormal(mu, sigam)
     *          */
    class RandNormal
    {
        public:
            typedef std::normal_distribution<double> DistType;
        private:
            /* Use mt19937 as the default random engine. */
            std::mt19937 random_engine;
            DistType dist;
        public:
            RandNormal(double mu, double sigma): random_engine(), dist(mu, sigma)
            {
                random_engine.seed(std::random_device()());
            }

            double gen() { return dist(random_engine); }

            double genOTF(double k, double theta)
            {
                return DistType(k, theta)(random_engine);
            }
    };

    /* generate random double numeber of Gamma distribution with
     * shape parameter k and scale parameter theta
     * Usage:
     *        RandoGamma(k, theta)
     *        */
    class RandGamma
    {
        public:
            typedef std::gamma_distribution<double> DistType;
        private:
            /* Use mt19937 as the default random engine. */
            std::mt19937 random_engine;
            DistType dist;
        public:
            RandGamma(double k, double theta): random_engine(), dist(k, theta)
            {
                random_engine.seed(std::random_device()());
            }

            double gen() { return dist(random_engine); }

            double genOTF(double k, double theta)
            {
                return DistType(k, theta)(random_engine);
            }
    };

    /* generate random double numeber of Gamma distribution with
     * shape parameter k and scale parameter theta
     * Usage:
     *        RandoPoisson(k)
     *        */
    class RandPoisson
    {
        public:
            typedef std::poisson_distribution<> DistType;
        private:
            /* Use mt19937 as the default random engine. */
            std::mt19937 random_engine;
            DistType dist;
        public:
            RandPoisson(double k): random_engine(), dist(k)
            {
                random_engine.seed(std::random_device()());
            }

            double gen() { return dist(random_engine); }

            double genOTF(double k)
            {
                return DistType(k)(random_engine);
            }
    };

    /* generate random double numeber of Gamma distribution with
     * mean lambda and variance sigma
     * Usage:
     *        RandoGamma(lambda, sigma)
     *        */
    class RandGammaMeanVar
    {
        public:
            double k;
            double theta;
            RandGamma rg;

        public:
            RandGammaMeanVar(double lambda, double sigma): k(lambda*lambda / (sigma*sigma)), theta(sigma*sigma / lambda), rg(k, theta)
            {
            }

            double gen() { return rg.gen(); }
    };


    /* generate random discrete integers of Gamma distribution with
     * mean lambda and variance sigma
     * Usage:
     *        RandoGamma(int lambda, double sigma)
     *        */
    class RandGammaMeanVarInt
    {
        public:
            RandGammaMeanVar rgmv;
        public:
            RandGammaMeanVarInt(int lambda, double sigma): rgmv(lambda - 1, sigma)
            {
            }

            int gen()
            {
                return std::round(rgmv.gen()) + 1;
            }
    };

    /* choose element from a vector randomly
     * Usage:
     *      vector<T> vec
     *      RandChoice<T> rc(vec)
     *      rc.choose()
     *      */
    template <typename T>
    class RandChoice
    {
        typedef std::vector<T> vectype;

        private:
            vectype & _vec; // vector to hold the choices
            RandInt randint;
        public:
            RandChoice(vectype & vec): _vec(vec), randint(vec.size() - 1)
            {
            }

            /* Randomly choose one of the element of the given vecotr */
            T& choose() { return _vec[randint.gen()]; }
    };

}
#endif
