#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <sstream>

namespace mylib
{
    /* For print stl containers */
    template <typename T>
    void printVec(std::vector<T> vec)
    {
        std::cout << "[";
        auto iter = vec.begin();
        if(iter != vec.end())
        {
            std::cout << *iter;
            ++iter;
        }

        for(;iter != vec.end(); ++iter)
            std::cout << ", " << *iter;

        std::cout << "]" << std::endl;
    }

    template <typename S, typename I>
    void printMap(std::map<S, I> m)
    {
        for(auto iter = m.begin(); iter != m.end(); ++iter)
        {
            std::cout << iter->first << " : " << iter->second << std::endl;
        }
    }

    template <typename T>
    void printSet(std::set<T> s)
    {
        if(s.empty())
        {
            std::cout << "set([])" << std::endl;
            return;
        }

        std::cout << "set([";
        auto iter = s.begin();
        if(iter != s.end())
        {
            std::cout << *iter;
            ++iter;
        }
        for(;iter != s.end(); ++iter)
            std::cout << ", " << *iter;
        std::cout << "])" << std::endl;

        return;
    }

    /* Overload << for stl containers */
    template <typename T>
    std::ostream & operator<<(std::ostream & out, std::vector<T> vec)
    {
        out << "[";
        auto iter = vec.begin();
        if(iter != vec.end())
        {
            out << *iter;
            ++iter;
        }

        for(;iter != vec.end(); ++iter)
            out << ", " << *iter;

        out << "]";
        return out;
    }

    template <typename T>
    std::ostream & operator<<(std::ostream & out, std::set<T> s)
    {
        if(s.empty())
        {
            out << "set([])";
            return out;
        }

        out << "set([";
        auto iter = s.begin();
        if(iter != s.end())
        {
            out << *iter;
            ++iter;
        }
        for(;iter != s.end(); ++iter)
            out << ", " << *iter;
        out << "])";

        return out;
    }

    template <typename S, typename I>
    std::ostream & operator<<(std::ostream & out, std::map<S, I> m)
    {
        if(m.size() == 0)
        {
            out << "{}";
            return out;
        }

        out << "{";
        auto iter = m.begin();
        if(iter != m.end())
        {
            out << iter->first << ": " << iter->second;
            ++iter;
        }

        for(; iter != m.end(); ++iter)
            out << ", " << iter->first << " : " << iter->second;
        out << "}";

        return out;
    }

    /* For strings */
    std::vector<std::string> &split(const std::string & str, std::vector<std::string> & elems, char delim = ' ');

    std::vector<std::string> splitString(const std::string & str, char delim = ' ');

} // end of namespace mylib

#endif
