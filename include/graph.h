#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <iostream>

using namespace std;

namespace mylib
{

    template <typename T>
    class TwoDLattice;

    /* node class for lattice class */
    template<typename T>
    class Node
    {
        private:
            int m_num;
            TwoDLattice<T> & m_lattice;
            vector<Node*> neighbors;

        public:
            T * content;

            /* Constructor */
            Node(int num, TwoDLattice<T> & lattice, T * a_content = 0): m_num(num), m_lattice(lattice), content(a_content)
            {
            }

            /* Attr methods. */
            /* get the number of this node in the lattice. */
            int getNum() const {return m_num;}

            /* Overloaded operators. */
            template <typename J>
            friend ostream& operator<<(ostream & out, Node<J> & node);

            /* Get neighbors. */
            vector<Node*>& getNeighbors()
            {
                return neighbors;
            }


            /* Set neighbors. */
            void setNeighbors()
            {
                int x, y; // the cordinates of the given node
                int n_x, n_y; // temp variable to hold neighbors' cordinates
                int L(m_lattice.getLength());

                /* Calculate the cordinates of the given node */
                x = m_num % L;
                y = m_num / L;

                /* left */
                if(x == 0)
                    n_x = L - 1;
                else
                    n_x = x - 1;
                n_y = y;

                neighbors.push_back(&m_lattice[n_y * L + n_x]);

                /* up */
                if(y==0)
                    n_y = L - 1;
                else
                    n_y = y - 1;
                n_x = x;

                neighbors.push_back(&m_lattice[n_y * L + n_x]);

                /* right */
                if(x==L-1)
                    n_x = 0;
                else
                    n_x = x + 1;
                n_y = y;

                neighbors.push_back(&m_lattice[n_y * L + n_x]);

                /* down */
                if(y==L-1)
                    n_y = 0;
                else
                    n_y = y + 1;
                n_x = x;

                neighbors.push_back(&m_lattice[n_y * L + n_x]);
            }

    };

    template <typename T>
    ostream& operator<<(ostream & out, Node<T> & node)
    {
        out << "<Node> num: " << node.getNum() << endl;
        return out;
    }


    template<typename T>
    class TwoDLattice
    {
        typedef typename vector<Node<T>>::iterator iterator;

        private:
            int m_length; // the length of the square lattice
            int size; // the size of the lattice
            vector<Node<T>> m_vec; // vector to store the nodes

        public:
            /* Constructor */
            TwoDLattice(int length): m_length(length), size(length * length)
            {
                for(int i=0; i<size; ++i)
                {
                    Node<T> node(i, *this);
                    m_vec.push_back(node);
                }

                for(int i=0; i<size; ++i)
                {
                    m_vec[i].setNeighbors();
                }

            }

            /* Attr methods */
            int getSize() { return size; }

            int getLength() { return m_length; }

            /* Iterator associated methods */
            iterator begin() { return m_vec.begin(); }

            iterator end() { return m_vec.end(); }

            /* Overload operators. */
            template <typename J>
            friend ostream & operator<<(ostream & out, TwoDLattice<J> & lattice);
            Node<T> & operator[](const int index)
            {
                return m_vec[index];
            }
    };

    template <typename T>
    ostream & operator<<(ostream & out, TwoDLattice<T> & lattice)
    {
        out << "<2 D lattice> with " << lattice.getSize() << " nodes." <<endl;
        return out;
    }

} // end of namespace mylib

#endif
