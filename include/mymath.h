#ifndef MYMATH_H
#define MYMATH_H

#include <iostream>
#include <cmath>
#include <vector>

namespace mylib
{
    // geometry
    class TwoDVector
    {
        private:
            double _x;
            double _y;
        public:
            TwoDVector(): _x(0), _y(0) {}
            TwoDVector(double x, double y): _x(x), _y(y) {}
            
            /* Attr methods */
            double getX() const { return _x; }
            double getY() const { return _y; }
            TwoDVector & setX(double x)
            {
                _x = x;
                return *this;
            }

            TwoDVector & setY(double y)
            {
                _y = y;
                return *this;
            }

            /* algebra */
            TwoDVector & addBy(const TwoDVector & other)
            {
                _x += other.getX();
                _y += other.getY();
                return *this;
            }
            TwoDVector & subtractBy(const TwoDVector & other)
            {
                _x -= other.getX();
                _y -= other.getY();
                return *this;
            }
            TwoDVector & dot(double value)
            {
                _x *= value;
                _y *= value;
                return *this;
            }
            /* Test and special case needed! */
            TwoDVector & diveideBy(double value)
            {
                if(fabs(value) < 1e-10)
                {
                    std::cerr << "TwoDVector divided by " << value << std::endl;
                    return *this;
                }
                _x /= value;
                _y /= value;
                return *this;
            }
            TwoDVector & normalize()
            {
                double base = _x * _x + _y * _y;
                base = sqrt(base);
                if(fabs(base) < 1e-10)
                {
                    std::cerr << "TwoDVector normalized by " << base << std::endl;
                    return *this;
                }

                _x /= base;
                _y /= base;

                return *this;
            }

            /* for distance */
            double distanceSquare(const TwoDVector & other) const
            {
                double d_x = other.getX() - _x ;
                double d_y = other.getY() - _y ;
                return d_x * d_x + d_y * d_y;
            }
            double distance(const TwoDVector & other) const
            {
                return sqrt(distanceSquare(other));
            }

            /* overloaded operators */
            friend TwoDVector operator+(const TwoDVector & one, const TwoDVector & two)
            {
                return TwoDVector(one.getX() + two.getX(), one.getY() + two.getY());
            }
            friend double operator*(const TwoDVector & one, const TwoDVector & two)
            {
                return one.getX() * two.getX() + one.getY() * two.getY();
            }
            TwoDVector & operator-()
            {
                _x = - _x;
                _y = - _y;
                return *this;
            }
            friend std::ostream & operator<<(std::ostream & out, const TwoDVector & vec)
            {
                out << "TwoDVector<" << vec.getX() << "," << vec.getY() << ">" << std::endl;
                return out;
            }
    };


    // numeric
    template <typename T>
    T sum(std::vector<T>& vec)
    {
        T acc(0);
        for(auto & iter: vec)
        {
            acc += iter;
        }
        return acc;
    }

    template <typename T>
    double average(std::vector<T>& vec)
    {
        return sum(vec) / static_cast<double>(vec.size());
    }

    // range functions
    std::vector<int> range(int start, int stop, int step)
    {
        if(step==0)
        {
            throw std::invalid_argument("Step for range must be non-zero");
        }

        std::vector<int> result;
        int i = start;
        while((step>0) ? (i<stop) : (i>stop))
        {
            result.push_back(i);
            i += step;
        }
        return result;
    }

    std::vector<int> range(int start, int stop)
    {
        return range(start, stop, 1);
    }

    std::vector<int> range(int stop)
    {
        return range(0, stop, 1);
    }
    
    std::vector<double> linspace(double start, double stop, int num)
    {
        double intervel = (stop - start) / static_cast<double>(num);
        double base = start;
        std::vector<double> result;
        for(int i=0; i<num; ++i)
        {
            result.push_back(base);
            base += intervel;
        }
        return result;
    }

    // simulation
    class SimWatcherConverge
    {
        private:
            int _number;
            double _epsilon;
            double counter;
            double last_one;
        public:
            SimWatcherConverge(int number, double epsilon, double init_value): _number(number), _epsilon(epsilon), counter(0), last_one(init_value)
            {
            }

            bool pushValue(double value)
            {
                if(std::abs(value - last_one) < _epsilon)
                    ++counter;
                else
                    counter = 0;
                last_one = value;
                return counter > _number;
            }
    };

    // TODO
//    class SimWatcherOscillate()
//    {
//        public:
//            SimWatcherOscillate(int number, double epsilon)
//    };
} // end of namespace mylib


#endif
