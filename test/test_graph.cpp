#include <vector>
#include "graph.h"
#include "utils.h"
#include <gtest/gtest.h>
#include <string>
#include <iostream>

using namespace std;
using namespace mylib;

TEST(Test_2dLattice, Test_Initializition)
{
    int n(10);
    TwoDLattice<string> lattice(n);

    auto iter = lattice.begin();
    for(int i=0; i<lattice.getSize(); i++)
    {
        EXPECT_EQ(iter->getNum(), i);
        iter++;
    }

    EXPECT_EQ(iter, lattice.end());

    for(int j=0; j<lattice.getSize(); j++)
    {
        EXPECT_EQ(j, lattice[j].getNum());
    }
}

TEST(Test_2dLattice, Test_methods)
{
    int n(10);
    TwoDLattice<int> lattice(n);
    EXPECT_EQ(lattice.getSize(), n*n);
    EXPECT_EQ(lattice.getLength(), n);
}

TEST(Test_2dLattice, Test_GetNeigebors)
{
    int n(10);
    TwoDLattice<string> lattice(n);

    /* Test for NO.0 */
    vector<int> neighbors_of_0_expect = {9, 90, 1, 10};
    auto neighbors_of_0 = lattice[0].getNeighbors();
    for(int i=0; i<4; i++)
    {
        EXPECT_EQ(neighbors_of_0_expect[i], neighbors_of_0[i]->getNum());
    }

    /* Test for NO.9 */
    vector<int> neighbors_of_9_expect = {8, 99, 0, 19};
    auto neighbors_of_9 = lattice[9].getNeighbors();
    for(int i=0; i<4; i++)
    {
        EXPECT_EQ(neighbors_of_9_expect[i], neighbors_of_9[i]->getNum());
    }

    /* Test for NO.55 */
    vector<int> neighbors_of_55_expect = {54, 45, 56, 65};
    auto neighbors_of_55 = lattice[55].getNeighbors();
    for(int i=0; i<4; i++)
    {
        EXPECT_EQ(neighbors_of_55_expect[i], neighbors_of_55[i]->getNum());
    }

    /* Test for NO.90 */
    vector<int> neighbors_of_90_expect = {99, 80, 91, 0};
    auto neighbors_of_90 = lattice[90].getNeighbors();
    for(int i=0; i<4; i++)
    {
        EXPECT_EQ(neighbors_of_90_expect[i], neighbors_of_90[i]->getNum());
    }
}
