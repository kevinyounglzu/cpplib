#include <gtest/gtest.h>
#include "parser.h"
#include <string>
#include <exception>
#include "utils.h"
#include <vector>
#include <string>

using namespace std;
using namespace mylib;

TEST(Test_parser, Test_initializition)
{
//    /* Test noexist file */
//    string str("./files/nonexist");
//    try
//    {
//        Parser ps(str);
//    }
//    catch(exception & e)
//    {
//        EXPECT_EQ(e.what(), string("Fail to open file"));
//    }

    string file_name("./test/files/config");
    Parser ps(file_name);
    EXPECT_EQ(file_name, ps.getFileName());
}

TEST(Test_parser, Test_read_file)
{
    vector<string> exp = {
        "int N=10",
        "double r=1.2",
        "int G=5"
    };
    string file_name("./test/files/content.cfg");
    Parser ps(file_name);
    for(int i=0; i<3; ++i)
    {
        EXPECT_EQ(ps.getVecOfStrings()[i], exp[i]);
    }
}

TEST(Test_parser, Test_parse_line)
{
    string file_name("./test/files/content.cfg");
    Parser ps(file_name);
    EXPECT_EQ(ps.getInt("N"), 10);
    EXPECT_DOUBLE_EQ(ps.getDouble("r"), 1.2);
    EXPECT_EQ(ps.getInt("G"), 5);
}

TEST(Test_Tape, Test_Tape)
{
    EXPECT_EQ(tapeFileName("./data/", ".out", 10), string("./data/10.out"));
    EXPECT_EQ(tapeFileName("./data/", ".out", 2.100), string("./data/2.1.out"));
    EXPECT_EQ(tapeFileName("./data/", ".out", "result"), string("./data/result.out"));
}
