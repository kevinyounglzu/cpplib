#include <iostream>
#include <gtest/gtest.h>
#include "rand.h"

using namespace std;
using namespace mylib;

TEST(Test_Rand, Test_Randint)
{
    RandInt randint(10);

    int rand;
    for(int i=0; i<1000; i++)
    {
        rand = randint.gen();
        EXPECT_LE(rand, 10);
        EXPECT_GE(rand, 0);
    }

    RandInt randint2(2, 5);

    for(int i=0; i<1000; i++)
    {
        rand = randint2.gen();
        EXPECT_LE(rand, 5);
        EXPECT_GE(rand, 2);
    }

    for(int i=0; i<1000; ++i)
    {
        int r = randint.genOTF(10, 20);
        EXPECT_LE(r, 20);
        EXPECT_GE(r, 10);
    }
}

TEST(Test_Rand, Test_RandDouble)
{
    double rand;

    RandDouble randd;
    for(int i=0; i<1000; i++)
    {
        rand = randd.gen();
        EXPECT_LT(rand, 1.0);
        EXPECT_GE(rand, 0.0);
    }

    RandDouble randd2(2.0);
    for(int i=0; i<1000; i++)
    {
        rand = randd2.gen();
        EXPECT_LT(rand, 2.0);
        EXPECT_GE(rand, 0.0);
    }

    RandDouble randd3(1.0, 3.0);
    for(int i=0; i<1000; i++)
    {
        rand = randd3.gen();
        EXPECT_LT(rand, 3.0);
        EXPECT_GE(rand, 1.0);
    }

    for(int i=0; i<1000; ++i)
    {
        double r = randd.genOTF(0, 0.5);
        EXPECT_LT(r, 0.5);
        EXPECT_GE(r, 0.0);
    }
}

TEST(Test_Rand, Test_RandChoice)
{
    vector<int> vec_1 = {0};
    RandChoice<int> randch1(vec_1);
    for(int i=0; i<1000; i++)
        EXPECT_EQ(randch1.choose(), 0);


    vector<int> vec_2 = {0, 1};
    RandChoice<int> randch2(vec_2);

    int n(0);
    for(int i=0; i<1000; i++)
    {
        n = randch2.choose();
        EXPECT_TRUE(n==0 || n==1);
    }

    vector<int> vec_3 = {0, 1, 2};
    RandChoice<int> randch3(vec_3);

    for(int i=0; i<1000; i++)
    {
        n = randch3.choose();
        EXPECT_TRUE(n==0 || n==1 || n==2);
    }
}
