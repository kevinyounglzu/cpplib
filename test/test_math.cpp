#include <iostream>
#include <gtest/gtest.h>
#include "mymath.h"
#include <vector>
#include "utils.h"

using namespace std;
using namespace mylib;
using mylib::operator<<;

TEST(Test_TwoDVector, Test_initializition)
{
    /* Default */
    TwoDVector vec;
    EXPECT_DOUBLE_EQ(vec.getX(), 0.);
    EXPECT_DOUBLE_EQ(vec.getY(), 0.);

    /* With parameters */
    TwoDVector vec2(10., 2.);
    EXPECT_DOUBLE_EQ(vec2.getX(), 10.);
    EXPECT_DOUBLE_EQ(vec2.getY(), 2.);

    /* Setting */
    vec.setX(10.);
    vec.setY(2.);
    EXPECT_DOUBLE_EQ(vec.getX(), 10.);
    EXPECT_DOUBLE_EQ(vec.getY(), 2.);
}

TEST(Test_TwoDVector, Test_addBy)
{
    TwoDVector vec;
    TwoDVector vec2(10., 2.);

    vec2.addBy(vec);
    EXPECT_DOUBLE_EQ(vec2.getX(), 10.);
    EXPECT_DOUBLE_EQ(vec2.getY(), 2.);

    vec.addBy(vec2);
    EXPECT_DOUBLE_EQ(vec.getX(), 10.);
    EXPECT_DOUBLE_EQ(vec.getY(), 2.);

    vec.addBy(vec2).addBy(vec2);
    EXPECT_DOUBLE_EQ(vec.getX(), 30.);
    EXPECT_DOUBLE_EQ(vec.getY(), 6.);
}

TEST(Test_TwoDVector, Test_subtractBy)
{
    TwoDVector vec(10., 20.);
    TwoDVector vec2(5., 4.);

    vec.subtractBy(vec2);
    EXPECT_DOUBLE_EQ(vec.getX(), 5.);
    EXPECT_DOUBLE_EQ(vec.getY(), 16.);

    vec.subtractBy(vec2).subtractBy(vec2);
    EXPECT_DOUBLE_EQ(vec.getX(), -5.);
    EXPECT_DOUBLE_EQ(vec.getY(), 8.);
}

TEST(Test_TwoDVector, Test_divided_by)
{
    TwoDVector vec(10., 20.);
    vec.diveideBy(2.);
    EXPECT_DOUBLE_EQ(vec.getX(), 5.);
    EXPECT_DOUBLE_EQ(vec.getY(), 10.);
    vec.diveideBy(1e-11);
    EXPECT_DOUBLE_EQ(vec.getX(), 5.);
    EXPECT_DOUBLE_EQ(vec.getY(), 10.);
    vec.diveideBy(0);
    EXPECT_DOUBLE_EQ(vec.getX(), 5.);
    EXPECT_DOUBLE_EQ(vec.getY(), 10.);
}

TEST(Test_TwoDVector, Test_operator_plus)
{
    TwoDVector one(1., 2.);
    TwoDVector two(3., 4.);
    TwoDVector result = one + two;
    EXPECT_DOUBLE_EQ(result.getX(), 1. + 3.);
    EXPECT_DOUBLE_EQ(result.getY(), 2. + 4.);
}

TEST(Test_TwoDVector, Test_negetive)
{
    TwoDVector one(1., 2.);
    -one;
    EXPECT_DOUBLE_EQ(one.getX(), -1.);
    EXPECT_DOUBLE_EQ(one.getY(), -2.);
}

TEST(Test_TwoDVector, Test_dot)
{
    TwoDVector vec(2., 4.);
    vec.dot(10);
    EXPECT_DOUBLE_EQ(vec.getX(), 20.);
    EXPECT_DOUBLE_EQ(vec.getY(), 40.);
}

TEST(Test_TwoDVector, Test_mutiply)
{
    TwoDVector vec(2., 4.);
    TwoDVector vec2(3., 5.);
    EXPECT_DOUBLE_EQ(vec * vec2, 2. * 3. + 4. * 5.);
}

TEST(Test_TwoDVector, Test_distance)
{
    TwoDVector vec(1., 2.);
    TwoDVector vec2(4., 6.);

    EXPECT_DOUBLE_EQ(vec.distanceSquare(vec), 0.);

    EXPECT_DOUBLE_EQ(vec.distanceSquare(vec2), 25.);
    EXPECT_DOUBLE_EQ(vec.distance(vec2), 5.);

    EXPECT_DOUBLE_EQ(vec2.distanceSquare(vec), 25.);
}

TEST(Test_TwoDVector, Test_normalize)
{
    TwoDVector vec(3., 4.);
    vec.normalize();
    EXPECT_DOUBLE_EQ(vec.getX(), 0.6);
    EXPECT_DOUBLE_EQ(vec.getY(), 0.8);

    vec.setX(0.).setY(0.);
    vec.normalize();
    EXPECT_DOUBLE_EQ(vec.getX(), 0.);
    EXPECT_DOUBLE_EQ(vec.getY(), 0.);
}

TEST(Test_Numeric, Test_Sum_average)
{
    // for integers
    vector<int> int_vec = {1, 2, 3, 4, 5};
    EXPECT_EQ(sum(int_vec), 15);
    EXPECT_DOUBLE_EQ(average(int_vec), 3.);

    // for doubles
    vector<double> double_vec = {0.1, 0.2, 0.3, 0.4, 0.5};
    EXPECT_DOUBLE_EQ(sum(double_vec), 1.5);
    EXPECT_DOUBLE_EQ(average(double_vec), .3);
}

TEST(Test_Range, Test_With_Step)
{
    vector<int> vec1 = range(10);
    vector<int> e_vec1 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    EXPECT_EQ(vec1.size(), e_vec1.size());
    for(int i=0; i<10; ++i)
    {
        EXPECT_EQ(vec1[i], e_vec1[i]);
    }

    vector<int> vec2 = range(5, 10);
    vector<int> e_vec2 = {5, 6, 7, 8, 9};
    EXPECT_EQ(vec2.size(), e_vec2.size());
    for(unsigned i=0; i<vec2.size(); ++i)
    {
        EXPECT_EQ(vec2[i], e_vec2[i]);
    }

    vector<int> vec3 = range(2, 10, 2);
    vector<int> e_vec3 = {2, 4, 6, 8};
    EXPECT_EQ(vec3.size(), e_vec3.size());
    for(unsigned i=0; i<vec3.size(); ++i)
    {
        EXPECT_EQ(vec3[i], e_vec3[i]);
    }

    vector<int> vec4 = range(10, 0, -2);
    vector<int> e_vec4 = {10, 8, 6, 4, 2};
    EXPECT_EQ(vec4.size(), e_vec4.size());
    for(unsigned i=0; i<vec4.size(); ++i)
    {
        EXPECT_EQ(vec4[i], e_vec4[i]);
    }
}

TEST(Test_Linspace, Test_All)
{
    vector<double> vec1 = linspace(0, 1, 10);
    vector<double> e_vec1 = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
    
    EXPECT_EQ(vec1.size(), e_vec1.size());
    for(unsigned i=0; i<vec1.size(); i++)
    {
        EXPECT_DOUBLE_EQ(vec1[i], e_vec1[i]);
    }
}

// Do not delete. Uncomment to see the effect
//TEST(Test_sim_watcher, Test_All)
//{
//    SimWatcherConverge swc(50, 0.0001, 1);
//    for(int i=1; i<10000; ++i)
//    {
//        double value = 1 / static_cast<double>(i);
//        cout << i << " " << value << endl;
//        if(swc.pushValue(value))
//            break;
//    }

//    SimWatcherConverge swc(50, 0.0001, 0);
//    for(int i=0; i<10000; ++i)
//    {
//        double value = sin(i*0.01);
//        cout << i << " " << value << endl;
//        if(swc.pushValue(value))
//            break;
//    }
//}
