#include <vector>
#include "utils.h"
#include <gtest/gtest.h>
#include <string>

using namespace std;
using mylib::operator<<;

TEST(Test_Utils, Test_print_Vec)
{
    vector<int> empty_vec;
    cout << "Empty vector: ";
    mylib::printVec(empty_vec);

    vector<int> test_int_vec = {1, 2, 3, 4, 5};
    cout << "Long vector: ";
    mylib::printVec(test_int_vec);
}

TEST(Test_Utils, Test_print_Set)
{
    set<int> empty_set;
    cout << "Empyt set: ";
    mylib::printSet(empty_set);

    set<int> int_set = {1, 2, 3, 4, 5, 4, 5, 1};
    mylib::printSet(int_set);

    set<string> string_set = {"good", "bad", "ok"};
    mylib::printSet(string_set);
}

TEST(Test_Utils, Test_split_string)
{
    string str("good bad ok");
    vector<string> exp = {
        "good",
        "bad",
        "ok"
    };
    auto vec_of_strings = mylib::splitString(str);
    for(int i=0; i<3; ++i)
    {
        EXPECT_EQ(vec_of_strings[i], exp[i]);
    }

    string empty;
    vec_of_strings = mylib::splitString(empty);
}


TEST(Test_Utils, Test_vector_operator)
{
    vector<int> empty_vec;
    cout << "Empty vector: " << empty_vec << endl;;

    vector<double> vec = {2., 2.2, 5.32, 19.20};
    cout << "Vector of doubles: " << vec << endl;
}

TEST(Test_Utils, Test_set_operator)
{
    set<int> empty_set;
    cout << "Empyt set: " << empty_set << endl;

    set<int> int_set = {1, 2, 3, 4, 5, 4, 5, 1};
    cout << "Set of int: " << int_set << endl;

    set<string> string_set = {"good", "bad", "ok"};
    cout << "Set of string: " << string_set << endl;
}

TEST(Test_Utils, Test_map_operator)
{
    map<int, string> empty_map;
    cout << "Empty map: " << empty_map << endl;
    
    map<int, string> int_string;
    int_string.insert(std::make_pair(1, "good"));
    int_string.insert(std::make_pair(2, "bad"));
    int_string.insert(std::make_pair(3, "ok"));
    cout << "Int string map: " << int_string << endl;

    map<string, double> string_int;
    string_int.insert(std::make_pair("good", 2.134));
    string_int.insert(std::make_pair("bad", 3.1415));
    string_int.insert(std::make_pair("ok", 10.9));
    cout << "String int map: " << string_int << endl;
}
