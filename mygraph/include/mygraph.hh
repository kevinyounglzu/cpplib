#ifndef MYGRAPH_H
#define MYGRAPH_H

#include <iostream>
#include <vector>
#include <list>

using namespace std;

namespace mygraph
{
    /* Forward deleration */
    template <typename T>
    class Node;

    template <typename T>
    class Edge;


    template <typename T>
    /* Class for abstract graph */
    class Graph
    {
        private:
            list<Node<T>> vec_of_nodes;
            list<Edge<T>> vec_of_edges;
        public:
            /* Constructors */
            Graph();

            /* Manipulation methods */
            void addNode(Node<T>&);
            void addEdge(Edge<T>&);
    };


    template <typename T>
    /* Class for the abstract node */
    class Node
    {
        private:
            vector<Node*> vec_of_neibors;
        public:
            /* content of the node contains */
            T content;
            /* Constructor */
            Node(T & other_T);
            /* Attr methods. */
            /* Overloaded operators. */
            template <typename J>
            friend ostream& operator<<(ostream & out, Node<J> & node);
    };

    template <typename T>
        /* Class for the abstract edge */
    class Edge
    {
        typedef Node<T>* pointer;
        private:
            pointer _one;
            pointer _two;
        public:
            vector<pointer> getNodesPtr();
            /* constructors */
            Edge(pointer one, pointer two);
    };

    /* ========================================================= */
    /* Defination of the classes */


    /* ---------------------------------------------------------- */
    /* Graph */
    template <typename T>
    Graph<T>::Graph(): vec_of_nodes(), vec_of_edges()
    {
    }


    /* ---------------------------------------------------------- */
    /* Node */
    template <typename T>
    Node<T>::Node(T & other_T): content(other_T), vec_of_neibors()
    {
    }

    /* ---------------------------------------------------------- */
    /* Edge */
    template <typename T>
    Edge<T>::Edge(pointer one, pointer two):_one(one), _two(two)
    {
    }

} // end of namespace mygraph

#endif
