# my graph lib

## 抽象设计

### 抽象类

- graph
- node
- edge

node和edge需要能够绑定信息才行。

### 存储结构

- vector<node>
- vector<edges>

## 接口

- iterator for nodes
- iterator for edges

## graph 生成函数

- 晶格
- 随机图
- 小世界网络
- 无标度网络

## graph 读入函数

- 邻接矩阵
- 邻接函数

## graph 算法

- 广度优先搜索
- 深度优先搜索
