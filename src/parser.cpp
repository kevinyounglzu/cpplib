#include "parser.h"
#include "utils.h"
#include <vector>
#include <string>

mylib::Parser::Parser(std::string file_name): _file_name(file_name), the_file(_file_name)
{
    if(!the_file.is_open())
    {
        the_file.close();
        std::cerr << "Fail to open file: " << file_name << std::endl;
        throw ("Fail to open file");
    }

    parseTheFile();
}

mylib::Parser & mylib::Parser::parseTheFile()
{
    readFile();
    for(auto str: vec_of_strings)
    {
        parseLine(str);
    }
    return *this;
}

mylib::Parser & mylib::Parser::readFile()
{
    std::string temp;
    while(std::getline(the_file, temp))
        vec_of_strings.push_back(temp);
    return *this;
}

/*
 * str should have such format:
 *     type name=vlaue
 */
void mylib::Parser::parseLine(const std::string str)
{
    std::vector<std::string> splited_string_1 = mylib::splitString(str, ' ');

    if(splited_string_1.size() != 2)
    {
        std::cerr << "Configure file: " << _file_name << " format wrong!" << std::endl;
        throw("Format wrong");
    }

    std::vector<std::string> splited_string_2 = mylib::splitString(splited_string_1[1], '=');

//    mylib::printVec(splited_string_2);

    if(splited_string_1[0] == "double")
    {
        map_double.insert(
                std::pair<std::string, double>(
                    splited_string_2[0],
                    std::stod(splited_string_2[1])
                    )
                );
    }
    else if(splited_string_1[0] == "int")
    {
        map_int.insert(
                std::pair<std::string, int>(
                    splited_string_2[0],
                    std::stoi(splited_string_2[1])
                    )
                );
    }
}
