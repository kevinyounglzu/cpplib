#include "utils.h"

std::vector<std::string> & mylib::split(const std::string & str, std::vector<std::string> & elems, char delim)
{
    std::stringstream ss(str);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> mylib::splitString(const std::string & str, char delim)
{
    std::vector<std::string> elems;
    split(str, elems, delim);
    return elems;
}
