CPPFLAGS=-Iinclude -std=c++11 -g -Wall -Wextra
GTEST=-lgtest
MYLIB=-Llib -lmycpp 

TEST_SRC=$(wildcard test/*.cpp)
TEST_OBJ=$(patsubst %.cpp,%.o,$(TEST_SRC))

LIB_SRC=$(wildcard src/*.cpp)
LIB_OBJ=$(patsubst %.cpp,%.o,$(LIB_SRC))
LIB=lib/libmycpp.a

HEADERS=$(wildcard include/*.h)
TEST=test/test
.PONY: tests clean build cleanall


tests: $(TEST)
	./$(TEST)

lib: $(LIB) clean

$(LIB): build $(LIB_OBJ)
	ar rcs $@ $(LIB_OBJ)
	ranlib $@

build:
	@mkdir -p lib

#$(TEST): CPPFLAGS+=-lgtest
$(TEST): $(LIB) $(TEST_OBJ) $(HEADERS)
	$(CXX) $(CPPFLAGS) $(MYLIB) $(GTEST) $(TEST_OBJ) -o $@

clean:
	rm -rf $(TEST) $(TEST_OBJ) $(LIB_OBJ)
	rm -rf `find . -name "*.dSYM" -print`

cleanall: clean
	rm -rf $(LIB)
